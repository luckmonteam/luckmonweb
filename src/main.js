import Vue from 'vue'
import App from './App.vue'
import VueMeta from 'vue-meta'
import vuetify from './plugins/vuetify'
import "vuetify/dist/vuetify.min.css";

Vue.config.productionTip = false
Vue.use(VueMeta)

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
